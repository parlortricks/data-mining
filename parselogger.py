#Copyright (c) 2015, Allan Stones
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib
import seaborn
import pandas as pd
import matplotlib.pyplot as plt
import codecs
import re
import sys
import time
import os

seaborn.set()

start_time = time.time()

def multiple_replace(dict, text):
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)

def print_stats():
	print("Processing time taken=" + str(round(time.time() - start_time, 3)) + " seconds")
	print("Input file size=" + str(round(filesize/1024/1024 ,2)) + " MiB")
	print("Lines dropped from source file=" + str(droppedLines) + "/" + str(totalLines))

replaceDict = {
	'"'					:	'',
	'\n'				:	'',
}

# Take first agrument on the command line as the CSV file to process
input_file = sys.argv[1]

filesize = os.stat(input_file).st_size

# Read the CSV file line for line and process with string replacement
syslog = []
droppedLines = 0
totalLines = 0

with codecs.open(input_file, 'r') as f:
	for line in f:        
		tmp = multiple_replace(replaceDict, line).split(',')
		if (len(tmp) <15):
			for item in tmp:
				item.strip()
			syslog.append(tmp)
		else:
			droppedLines = droppedLines +1
		totalLines = totalLines +1
			

        
# Create a DataFrame, skipping the header line
df = pd.DataFrame(syslog[1:])



# Create the column names
df.columns = [
			'date time',
			'virtual server',
			'http refer',
			'client ip',
			'http method',
			'http url',
			'http uri',
			'http version',
			'http status',
			'content length',
			'lb server pool',
			'lb server ip',
			'lb server port',
			'response time',
            ]

df['date time'] = pd.to_datetime(df['date time']) # Convert to DateTime format
df['response time'] = df['response time'].astype(float) # Convert to a float number

#print(df.describe())
#print(df.head(5))
#print(df.info())

# Line graph of response times
fig, ax = plt.subplots(figsize=(16,8))
df1=df[df['response time'] < 30000].plot(title="Response Times", x="date time", y="response time", ax=ax)
df1.set_ylabel("Milliseconds")
df1.set_xlabel("Time")
fig.tight_layout()
fig.savefig("response_times.png")

# Bar graph of top 25 client ip based on number of requests
fig, ax = plt.subplots(figsize=(16,8))
df2=df['client ip'].value_counts().head(25).plot(title="Top 25 Client IP", kind='bar', align='center', ax=ax)
df2.set_ylabel("# of Requests")
df2.set_xlabel("Client IP")
fig.tight_layout()
fig.savefig("top_25.png")

# Line graph of response times grouped 1minute
ts = df.set_index(['date time'])
#print(ts.head(10))

fig, ax = plt.subplots(figsize=(16,8))
df3=ts.groupby(ts.index.minute).mean().plot(title="Reponse Time - 1minute", ax=ax)
df3.set_ylabel("Milliseconds")
df3.set_xlabel("Time")
fig.tight_layout()
fig.savefig("response_times_1min.png")

# Line graph of response times
fig, ax = plt.subplots(figsize=(16,8))
df4=df[(df['response time'] > 1000) & (df['response time'] < 30000) ].plot(title="Response Times >1s <30s", x="date time", y="response time", ax=ax)
df4.set_ylabel("Milliseconds")
df4.set_xlabel("Time")
fig.tight_layout()
fig.savefig("response_times_gt_1s_lt_30s.png")

# Line graph of response times
fig, ax = plt.subplots(figsize=(16,8))
df5=df[df['response time'] > 30000].plot(title="Response Times > 30s", x="date time", y="response time", ax=ax)
df5.set_ylabel("Milliseconds")
df5.set_xlabel("Time")
fig.tight_layout()
fig.savefig("response_times_gt_30s.png")

print_stats()